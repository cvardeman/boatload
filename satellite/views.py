import json
import base64
import sys
import traceback

from rest_framework.response import Response
from rest_framework import permissions
from rest_framework import views
from rest_framework.renderers import JSONRenderer, BaseRenderer
import sc
import sc
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

from boatload.settings import BASE_sc_URL


# Create your views here.
class ListOnHost(views.APIView):
    """
    Returns a list of all running sc containers.


    Sub Endpoints:


    - ./&lt;container id&gt;/metrics - *Returns metrics for the specified container.*
    - ./&lt;container id&gt;/stdout - *Returns the log output for the specified container.*
    - ./&lt;container id&gt;/changes - *Returns a list of changed files for the specified container. POST ```{"path":"filepath"} to get a file*
    - ./&lt;container id&gt;/changes/&lt;filename&gt; - *Returns the selected file from the specified container in base64 encoding.*
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        dcli = sc.Client(base_url=BASE_sc_URL, version='auto')

        try:
            containers = dcli.containers()
        except Exception as e:
            return Response({"error": sys.exc_info()[1], "traceback": traceback.format_exc()})
        return Response({"containers": containers})


class Metrics(views.APIView):
    """
    Returns metrics for the specified container.
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, id):
        dcli = sc.Client(base_url=BASE_sc_URL, version='auto')

        try:
            logs = dcli.inspect_container(id)
        except Exception as e:
            return Response({"error": sys.exc_info()[1], "traceback": traceback.format_exc()})
        return Response({"stdout": logs})


class StdOut(views.APIView):
    """
    Returns the log output for the specified container.
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, id):
        dcli = sc.Client(base_url=BASE_sc_URL, version='auto')

        try:
            data = dcli.logs(id)
        except Exception as e:
            return Response({"error": sys.exc_info()[1], "traceback": traceback.format_exc()})
        return Response(json.dumps(data.decode('utf-8')))


class BinOrJsonRenderer(BaseRenderer):
    media_type = 'application/x-tar'
    format = 'tar'
    charset = None
    render_style = 'binary'

    def render(self, data=None, accepted_media_type=None, renderer_context=None):
        if isinstance(data, bytes):
            return data
        else:
            jr = JSONRenderer()
            self.media_type = jr.media_type
            self.format = jr.format
            self.charset = jr.charset
            self.render_style = jr.render_style
            return jr.render(data, accepted_media_type=accepted_media_type, renderer_context=renderer_context)


class Diff(views.APIView):
    """
    Returns a list of changed files for the specified container.
    """
    permission_classes = (permissions.AllowAny,)
    renderer_classes = (BinOrJsonRenderer,)

    def get(self, request, id):
        dcli = sc.Client(base_url=BASE_sc_URL, version='auto')

        try:
            data = dcli.diff(id)
        except Exception as e:
            return Response({"error": sys.exc_info()[1], "traceback": traceback.format_exc()})
        return Response(json.dumps(data))

    def post(self, request, id, *args, **kwargs):
        data = request.data
        if 'path' in data:
            dcli = sc.Client(base_url=BASE_sc_URL, version='auto')

            try:
                data = dcli.copy(id, data['path']).data
                response = Response(data, content_type='application/x-tar')
                response['Content-Disposition'] = 'attachment; filename="data.tar"'
                return response
            except Exception as e:
                return Response({"error": sys.exc_info()[1], "traceback": traceback.format_exc()})

        return Response("No path specified",
                        status=status.HTTP_400_BAD_REQUEST)


class Download(views.APIView):
    """
    Returns the selected file from the specified container in base64 encoding.
    """
    permission_classes = (permissions.AllowAny,)
    renderer_classes = (BinOrJsonRenderer,)

    def get(self, request, id, path):
        dcli = sc.Client(base_url=BASE_sc_URL, version='auto')

        try:
            data = dcli.copy(id, path).data
            response = Response(data, content_type='application/x-tar')
            response['Content-Disposition'] = 'attachment; filename="data.tar"'
            return response
        except Exception as e:
            return Response({"error": sys.exc_info()[1], "traceback": traceback.format_exc()})


class DownloadArchive(views.APIView):
    """
    Returns the selected file from the specified container in base64 encoding.
    """
    permission_classes = (permissions.AllowAny,)
    renderer_classes = (BinOrJsonRenderer,)

    def get(self, request, id):
        dcli = sc.Client(base_url=BASE_sc_URL, version='auto')

        try:
            data = dcli.get_archive(id, '/output').data
            response = Response(data, content_type='application/x-tar')
            response['Content-Disposition'] = 'attachment; filename="data.tar"'
            return response
        except Exception as e:
            return Response({"error": sys.exc_info()[1], "traceback": traceback.format_exc()})

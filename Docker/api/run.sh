#!/bin/sh

if [ ! -z "$ADMINHASH" ]
then
    sed "s/##PWHASH##/$ADMINHASH/g" /app/data/data.json.template > /app/data/data.json
fi

/env/bin/python3 /app/manage.py migrate --noinput
/env/bin/python3 /app/manage.py loaddata /app/data/data.json
/env/bin/python3 /app/manage.py runserver 0.0.0.0:8080
#!/bin/bash

cd base
sh ./build.sh || { echo 'base failed' ; exit 1; }
docker push keyz182/boatload_base:latest || { echo 'base failed' ; exit 1; }
cd ../api
sh ./build.sh || { echo 'api failed' ; exit 1; }
docker push keyz182/boatload_api:latest || { echo 'api failed' ; exit 1; }
cd ../satellite
sh ./build.sh || { echo 'satellite failed' ; exit 1; }
docker push keyz182/boatload_satellite:latest || { echo 'satellite failed' ; exit 1; }
cd ../satellite-discovery
sh ./build.sh || { echo 'satellite-discovery failed' ; exit 1; }
docker push keyz182/boatload_satellite-discovery:latest || { echo 'satellite-discovery failed' ; exit 1; }
cd ..


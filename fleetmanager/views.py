__author__ = 'keyz'
import base64
import sys
import traceback
import json

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
import urllib.parse
from builtins import isinstance
from django.contrib.auth.models import AnonymousUser
from django.http import HttpResponseRedirect
from fleetmanager.models import Job
from fleetmanager.serializers import JobSerializer

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework import status
from rest_framework import mixins
from rest_framework import generics
from rest_framework.generics import get_object_or_404
from rest_framework.renderers import JSONRenderer, BaseRenderer, BrowsableAPIRenderer

from fleetmanager.fleettools import create_job_fleet, start_job_fleet, get_job_state_fleet, delete_job_fleet, \
    get_all_job_states
from fleetmanager.satellitetools import get_stdout, get_metrics, get_changes, get_changed_file, get_archive
from fleetmanager.filters import IsOwnerFilterBackend, IsNotDeletedFilterBackend


##Fleet
# GET /container/<id> - Get Info
# GET /container/ - List containers
# POST /container - Create Job
# - Create unit file from template, use django templates.
# - Name with uuid?
# - Stick info in etcd
# DELETE /container/<id> - Stop container
# - delete unit via id
# - remove from etcd
class Jobs(mixins.ListModelMixin,
           mixins.CreateModelMixin,
           mixins.RetrieveModelMixin,
           mixins.DestroyModelMixin,
           generics.GenericAPIView):
    """
    To create a Job, you must POST scriptname, script,
    image_name and image_tag (all strings).

    You will receive an ID in return.

    GET /container/ - List containers

    POST /container - Create Job

    - Create unit file from template, use django templates.

    - Name with uuid?

    - Stick info in etcd

    GET /container/&lt;container id&gt; - Get Info

    DELETE /container/&lt;container id&gt; - Stop container

    - delete unit via id

    - remove from etcd
    """
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,IsNotDeletedFilterBackend,)
    lookup_field="container_id"

    def perform_destroy(self, instance):
        delete_job_fleet(instance.container_id)
        instance.is_deleted = True
        instance.save()

    def perform_create(self, serializer, owner):
        obj = serializer.save(owner=owner)
        start_job_fleet(obj.container_id)


    def get(self, request, *args, **kwargs):
        if "container_id" in kwargs:
            return self.retrieve(request, *args, **kwargs)
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = request.data

        if 'scriptname' not in data:
            data['scriptname'] = 'script.py'

        if 'image_name' not in data:
            return Response("No image_name",
                            status=status.HTTP_400_BAD_REQUEST)

        if 'image_tag' not in data:
            return Response("No image_tag",
                            status=status.HTTP_400_BAD_REQUEST)

        if 'dataurl' not in data:
            data['dataurl'] = None
        if 'datapath' not in data:
            data['datapath'] = None
        if 'scripturl' not in data:
            data['scripturl'] = None
        if 'container_args' not in data:
            data['container_args'] = None
        if 'restart' not in data:
            data['restart'] = None
        if 'restartsec' not in data:
            data['restartsec'] = None

        jobdata = create_job_fleet(image_name=data['image_name'], image_tag=data['image_tag'],
                                   scriptname=data['scriptname'], scripturl=data['scripturl'],
                                   dataurl=data['dataurl'], datapath=data['datapath'],
                                   container_args=data['container_args'], restart=data['restart'],
                                   restartsec=data['restartsec'])

        data.update(jobdata)

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer, self.request.user)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class State(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field = "container_id"

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        return Response(get_job_state_fleet(instance.container_id))


class Script(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field = "container_id"

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        return HttpResponseRedirect(instance.script.url)


class Data(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field = "container_id"

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        return HttpResponseRedirect(instance.data.url)

# ##DockerSatellite
# - Get using container id
# GET /container/<id>/stdout - Get StdOut
# GET /container/<id>/metrics - Get Metric
# GET /container/<id>/changes - Get Changes POST ```{"path":"filepath"} to get a file
# GET /container/<id>/changes/filenumorpath? - Get file at path

class StdOut(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field="container_id"

    def get(self, request, *args, **kwargs):
        instance = self.get_object()

        try:
            return Response(get_stdout(instance.container_id))
        except Exception as e:
            return Response({"error": sys.exc_info()[1], "traceback": traceback.format_exc()},
                            status=status.HTTP_400_BAD_REQUEST)

class Metrics(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field="container_id"

    def get(self, request, *args, **kwargs):
        instance = self.get_object()

        try:
            return Response(get_metrics(instance.container_id))
        except Exception as e:
            return Response({"error": sys.exc_info()[1], "traceback": traceback.format_exc()},
                            status=status.HTTP_400_BAD_REQUEST)


class Changes(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field = "container_id"

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            return Response(get_changes(instance.container_id))
        except Exception as e:
            return Response({"error": sys.exc_info()[1], "traceback": traceback.format_exc()},
                            status=status.HTTP_400_BAD_REQUEST)


class TarRenderer(BaseRenderer):
    media_type = 'application/x-tar'
    format = 'tar'
    charset = None
    render_style = 'binary'

    def render(self, data, media_type=None, renderer_context=None):
        return data


class GetFileByNum(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field = "container_id"
    renderer_classes = (TarRenderer, BrowsableAPIRenderer,)

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        out = None
        file = None
        try:
            num = int(kwargs['filenum'])
            out = json.loads(get_changes(instance.container_id))
            file = out[num]['Path']
            response = Response(get_changed_file(instance.container_id, file), content_type='application/x-tar')
            response['Content-Disposition'] = 'attachment; filename="data.tar"'
            return response
        except Exception as e:
            return Response(
                json.dumps({"error": sys.exc_info()[1], "traceback": traceback.format_exc(), 'data': [file, out]}),
                status=status.HTTP_400_BAD_REQUEST)


class GetFile(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field="container_id"
    renderer_classes = (TarRenderer,)

    def get(self, request, *args, **kwargs):
        instance = self.get_object()

        if 'path' in kwargs:
            filepath = kwargs['path']
            path = urllib.parse.unquote(filepath).decode('utf8')
            response = Response(get_changed_file(instance.container_id, path), content_type='application/x-tar')
            response['Content-Disposition'] = 'attachment; filename="data.tar"'
            return response
        else:
            return Response(json.dumps({"error": sys.exc_info()[1], "traceback": traceback.format_exc()}),
                            status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, *args, **kwargs):
        data = request.data
        instance = self.get_object()
        if 'path' in data:
            response = Response(get_changed_file(instance.container_id, data['path']), content_type='application/x-tar')
            response['Content-Disposition'] = 'attachment; filename="data.tar"'
            return response
        else:
            return Response({"error": sys.exc_info()[1], "traceback": traceback.format_exc()},
                            status=status.HTTP_400_BAD_REQUEST)


class Archive(generics.GenericAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (IsOwnerFilterBackend,)
    lookup_field = "container_id"
    renderer_classes = (TarRenderer,)

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        response = Response(get_archive(instance.container_id), content_type='application/x-tar')
        response['Content-Disposition'] = 'attachment; filename="data.tar"'
        return response

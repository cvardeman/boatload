from builtins import Exception, list, tuple

__author__ = 'keyz'
import etcd
from etcd import EtcdException, EtcdKeyNotFound
import requests
import traceback

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
from fleetmanager.models import Job
from boatload.settings import ETCDHOST, ETCDPATH, ETCDPORT


class SatelliteException(Exception):
    def __init__(self, message, traceback=''):
        # Call the base class constructor with the parameters it needs
        super(SatelliteException, self).__init__(message)

        # Now for your custom code...
        self.traceback = traceback


def return_json_or_raise(r):
    try:
        r.raise_for_status()
        return r.json()
    except Exception as e:
        newex = None
        try:
            return r.json()
        except:
            return {'error': r.content, 'traceback': traceback.format_exc()}


def return_file_or_raise(r):
    try:
        r.raise_for_status()
        return r.raw.data
    except Exception as e:
        try:
            return r.json()
        except:
            return {'error': r.content, 'traceback': traceback.format_exc()}


def get_stdout(id):
    template = "http://%s/v1/satellite/%s/stdout/"
    job = Job.objects.get(container_id=id)
    etc = etcd.Client(host=ETCDHOST, port=ETCDPORT)
    path = "".join([ETCDPATH, job.host])

    try:
        satellite = etc.read(path)
    except EtcdKeyNotFound:
        return {'error': "key not found in etcd"}
    except EtcdException as e:
        return {"error": e.message, "payload": e.payload}

    url = template % (satellite.value, job.container_id)

    r = requests.get(url)

    return return_json_or_raise(r)

def get_metrics(id):
    template = "http://%s/v1/satellite/%s/metrics/"
    job = Job.objects.get(container_id=id)
    etc = etcd.Client(host=ETCDHOST, port=ETCDPORT)
    path = "".join([ETCDPATH, job.host])

    try:
        satellite = etc.read(path)
    except EtcdKeyNotFound:
        return {'error': "key not found in etcd"}
    except EtcdException as e:
        return {"error": e.message, "payload": e.payload}

    url = template % (satellite.value, job.container_id)

    r = requests.get(url)

    return return_json_or_raise(r)


def get_changes(id):
    template = "http://%s/v1/satellite/%s/changes/"
    job = Job.objects.get(container_id=id)
    etc = etcd.Client(host=ETCDHOST, port=ETCDPORT)
    path = "".join([ETCDPATH, job.host])

    try:
        satellite = etc.read(path)
    except EtcdKeyNotFound:
        return {'error': "key not found in etcd"}
    except EtcdException as e:
        return {"error": e.message, "payload": e.payload}

    url = template % (satellite.value, job.container_id)

    r = requests.get(url)

    return return_json_or_raise(r)


def get_archive(id):
    template = "http://%s/v1/satellite/%s/archive/"
    job = Job.objects.get(container_id=id)
    etc = etcd.Client(host=ETCDHOST, port=ETCDPORT)
    path = "".join([ETCDPATH, job.host])

    try:
        satellite = etc.read(path)
    except EtcdKeyNotFound:
        return {'error': "key not found in etcd"}
    except EtcdException as e:
        return {"error": e.message, "payload": e.payload}

    url = template % (satellite.value, job.container_id)

    r = requests.get(url=url)

    return return_file_or_raise(r)

def get_changed_file(id, file):
    template = "http://%s/v1/satellite/%s/changes/"
    job = Job.objects.get(container_id=id)
    etc = etcd.Client(host=ETCDHOST, port=ETCDPORT)
    path = "".join([ETCDPATH, job.host])

    try:
        satellite = etc.read(path)
    except EtcdKeyNotFound:
        return {'error': "key not found in etcd"}
    except EtcdException as e:
        return {"error": e.message, "payload": e.payload}

    url = template % (satellite.value, job.container_id)
    payload = {"path": file}

    r = requests.post(url=url, data=payload, stream=True)

    try:
        r.raise_for_status()
        return r.raw.data
    except Exception as e:
        try:
            return r.json()
        except:
            return {'error': r.content, 'traceback': traceback.format_exc()}

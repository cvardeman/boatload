#!/usr/bin/python3
import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'boatload.settings'
from django.contrib.auth.hashers import make_password

pwmatch = False
while pwmatch == False:
    pw = input("Enter password: ")
    pwcheck = input("Enter password again: ")

    if pw == pwcheck:
        pwmatch = True
    else:
        print("Passwords don't match, try again.")

salt = os.urandom(8).hex()  # 64-bit salt.
hash = make_password(pw, salt=salt, hasher="pbkdf2_sha256")

print(hash)

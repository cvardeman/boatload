import requests

DOCKER_API_URL = 'http://131.251.172.65:8080/v1/container/'

params = dict(
    format='json',
)

script = "https://dbyz.co.uk/Public/test.sh"
data = "https://dbyz.co.uk/Public/cloudinit.zip"
datapath = "/data123/"
scriptname = "test.sh"

payload = {
    "image_name": "ubuntu",
    "image_tag": "latest",
    "scriptname": scriptname,
    "scripturl": script,
    "dataurl": data,
    "datapath": datapath,
    "container_args": "sh " + datapath + scriptname
}

resp = requests.post(url=DOCKER_API_URL, params=params, data=payload, auth=('admin', 'admin'))

print(resp.text)
